from javax.servlet.http import HttpServlet
class RedirectServlet(HttpServlet):
  def doGet(self, req, resp):
    if req.getParameter('help'):
      resp.getOutputStream().write(u'url - the destination of the redirect\nuser - the username making the request, defaults to the authenticated user.\n')
      return
    from java.lang import System
    clickTracking = file((u'%s/public_html/track.csv') % (System.getProperty(u'user.home')), u'a')
    from org.joda.time import DateTime
    user = req.getRemoteUser()
    if user is None:
      user = req.getParameter('user')
    clickTracking.write((u'%s, %s, %s\n') % (DateTime().getMillis(), user, req.getParameter(u'url')))
    clickTracking.close()
    resp.sendRedirect(req.getParameter(u'url'))
